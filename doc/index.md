---
title: Home
---

# rio-etc-gtk-template - A simple application template using RIO, etc and GTK

This is the soft documentation site for the rio-etc-gtk-template.

This serves as a place to provide detailed descriptions about how to
use your application or library.

![build](https://gitlab.com/zenhaskell/rio-etc-gtk-template/badges/master/build.svg)
