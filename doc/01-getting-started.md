---
title: Getting Started
---

rio-etc-gtk-template has two commands, `config` and `run`. Here are some example use cases

```bash
app-exe config
```

```bash
app-exe run
```
